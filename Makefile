

  

x11-screenshot:
	  cc -lX11        source/x11/x11-screenshot-ppm.c       -o source/bin/x11-screenshot-ppm    
	  cc -lX11 -lpng  source/x11/x11-screenshot-png.c   -o source/bin/x11-screenshot-png    

screenshot:
	  cc -lX11        source/x11/x11-screenshot-ppm.c   -o source/bin/x11-screenshot-ppm    
	  cc -lX11 -lpng  source/x11/x11-screenshot-png.c   -o source/bin/x11-screenshot-png    
	  ./source/bin/x11-screenshot-ppm   > /tmp/screenshot.ppm   
	  ./source/bin/x11-screenshot-png   


clean:
	  rm ./source/bin/x11-screenshot-ppm  
	  rm ./source/bin/x11-screenshot-png   

