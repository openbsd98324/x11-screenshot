
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <X11/Xlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <X11/Xutil.h> // Xgetpixel
#define PNG_DEBUG 3
#include <png.h>
int print_ppm = 0;  // in case you need


int main(int argc, char **argv)
{
	const char *display_name;
	if (argc > 2 || (argc == 2 && argv[1][0] == '-')) {
		fprintf(stderr, "Usage: %s [display] >screenshot.ppm\n", argv[0]);
		return 2;
	}
	if (argc >= 2) {
		display_name = argv[1];
	} else {
		display_name = getenv("DISPLAY");
		if (!display_name || !*display_name) {
			fprintf(stderr, "DISPLAY environment variable not set, can't take"
					" screenshot\n");
			return 1;
		}
	}

	Display *display = XOpenDisplay(display_name);
	if (!display) {
		fprintf(stderr, "Unable to open display %s\n", display_name);
		return 1;
	}

	const int screen = XDefaultScreen(display);
	const int width = DisplayWidth(display, screen);
	const int height = DisplayHeight(display, screen);
	const Window window = RootWindow(display, screen);

	unsigned long red_mask, green_mask, blue_mask;
	int red_shift, green_shift, blue_shift;
	int red_bits, green_bits, blue_bits;
	XWindowAttributes window_attr;
	if (!XGetWindowAttributes(display, window, &window_attr)) {
		fprintf(stderr, "Unable to retrieve root window attributes\n");
		XCloseDisplay(display);
		return 1;
	}
	if (window_attr.visual->class != TrueColor) {
		fprintf(stderr, "Visual class %d unsupported\n",
				window_attr.visual->class);
	}

	red_mask = window_attr.visual->red_mask;
	green_mask = window_attr.visual->green_mask;
	blue_mask = window_attr.visual->blue_mask;

	XImage *image = XGetImage(display, window, 0, 0, width, height, AllPlanes, ZPixmap);
	if (!image) {
		fprintf(stderr, "Unable to copy root window image\n");
		XCloseDisplay(display);
		return 1;
	}
	if (image->bitmap_unit != 32) {
		fprintf(stderr, "%dbpp display not supported\n", image->bitmap_unit);
		XDestroyImage(image);
		XCloseDisplay(display);
		return 1;
	}

	const long npixels = (long)width * (long)height;
	unsigned char *pixels = malloc(npixels*6);
	if (!pixels) 
	{
		fprintf(stderr, "Unable to allocate memory for pixel data (%dx%d)\n",
				width, height);
		XDestroyImage(image);
		XCloseDisplay(display);
		return 1;
	}
	unsigned int maxval;

	if (red_bits > 8 || green_bits > 8 || blue_bits > 8) {
		maxval = 65535;
		const uint32_t *in = (const uint32_t *)image->data;
		unsigned char *out = pixels;
		for (int y = 0; y < height; y++, in += image->bytes_per_line/4) {
			for (int x = 0; x < width; x++, out += 6) {
				const uint32_t pixel = in[x];
				unsigned long red = (pixel >> red_shift) & red_mask;
				unsigned long green = (pixel >> green_shift) & green_mask;
				unsigned long blue = (pixel >> blue_shift) & blue_mask;
				red = (red << (16-red_bits)) | (red >> (2*red_bits-16));
				green = (green << (16-green_bits)) | (green >> (2*green_bits-16));
				blue = (blue << (16-blue_bits)) | (blue >> (2*blue_bits-16));
				out[0] = red >> 8;
				out[1] = red >> 0;
				out[2] = green >> 8;
				out[3] = green >> 0;
				out[4] = blue >> 8;
				out[5] = blue >> 0;
			}
		}
	} else {
		maxval = 255;
		const uint32_t *in = (const uint32_t *)image->data;
		unsigned char *out = pixels;
		for (int y = 0; y < height; y++, in += image->bytes_per_line/4) {
			for (int x = 0; x < width; x++, out += 3) {
				const uint32_t pixel = in[x];
				unsigned long red = (pixel >> red_shift) & red_mask;
				unsigned long green = (pixel >> green_shift) & green_mask;
				unsigned long blue = (pixel >> blue_shift) & blue_mask;
				red = (red << (8-red_bits)) | (red >> (2*red_bits-8));
				green = (green << (8-green_bits)) | (green >> (2*green_bits-8));
				blue = (blue << (8-blue_bits)) | (blue >> (2*blue_bits-8));
				out[0] = red;
				out[1] = green;
				out[2] = blue;
			}
		}
	}





	// new 
	int code = 0;
	FILE *fp;
	png_structp png_ptr;
	png_infop png_info_ptr;
	png_bytep png_row;
	// Open file
	fp = fopen ("/tmp/test.png", "wb");
	if (fp == NULL){
		fprintf (stderr, "Could not open file for writing\n");
		code = 1;
	}
	// Initialize write structure
	png_ptr = png_create_write_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	// Initialize info structure
	png_info_ptr = png_create_info_struct (png_ptr);

	if (png_ptr == NULL){
		fprintf (stderr, "Could not allocate write struct\n");
		code = 1;
	}

	if (png_info_ptr == NULL){
		fprintf (stderr, "Could not allocate info struct\n");
		code = 1;
	}

	// Setup Exception handling
	if (setjmp (png_jmpbuf (png_ptr))){
		fprintf(stderr, "Error during png creation\n");
		code = 1;
	}



	png_init_io (png_ptr, fp);
	// Write header (8 bit colour depth)
	png_set_IHDR (png_ptr, png_info_ptr, width, height, 8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
	// Set title
	char *title = "Screenshot";
	if (title != NULL){
		png_text title_text;
		title_text.compression = PNG_TEXT_COMPRESSION_NONE;
		title_text.key = "Title";
		title_text.text = title;
		png_set_text (png_ptr, png_info_ptr, &title_text, 1);
	}
	png_write_info (png_ptr, png_info_ptr);
	// Allocate memory for one row (3 bytes per pixel - RGB)
	png_row = (png_bytep) malloc (3 * width * sizeof (png_byte));
	// Write image data
	int x, y;
	for (y = 0; y < height; y++){
		for (x = 0; x < width; x++){
			unsigned long pixel = XGetPixel (image, x, y);
			unsigned char blue = pixel & blue_mask;
			unsigned char green = (pixel & green_mask) >> 8; 
			unsigned char red = (pixel & red_mask) >> 16;
			png_byte *ptr = &(png_row[x*3]);
			ptr[0] = red;
			ptr[1] = green;
			ptr[2] = blue;
		}
		png_write_row (png_ptr, png_row);
	}
	// End write
	png_write_end (png_ptr, NULL);
	// Free
	fclose (fp);
	if (png_info_ptr != NULL) png_free_data (png_ptr, png_info_ptr, PNG_FREE_ALL, -1);
	if (png_ptr != NULL) png_destroy_write_struct (&png_ptr, (png_infopp)NULL);
	if (png_row != NULL) free (png_row);



	XDestroyImage(image);
	XCloseDisplay(display);

	if (printf("P6\n%d %d 255\n", width, height) < 0 ) {
		perror("Unable to write PPM header");
		free(pixels);
		return 1;
	}


	if ( print_ppm == 1 ) 
		if (fwrite(pixels, maxval==65535 ? 6 : 3, npixels, stdout) != npixels) 
		{
			perror("Unable to write image data");
			free(pixels);
			return 1;
		}

	free(pixels);
	return 0;
}






